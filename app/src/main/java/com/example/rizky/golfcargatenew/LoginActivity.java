package com.example.rizky.golfcargatenew;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class LoginActivity extends AppCompatActivity {

    Button jBtnTestSignIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        jBtnTestSignIn = (Button) findViewById(R.id.btnTestIntent);

        jBtnTestSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent testIntent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(testIntent);
            }
        });
    }
}
